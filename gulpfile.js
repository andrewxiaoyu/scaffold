'use strict';

const { watch, src, dest, series } = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");

// Change to prefered sass compiler
sass.compiler = require('node-sass');

function minifyCss(cb){
    src('css/**/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(dest('min/'));
    cb();
}

function compileSass(cb) {
    src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('css/'));
    cb();
}

function watchSass(cb) {
    watch('sass/**/*.scss', series(compileSass, minifyCss));
    cb();
}

exports.minify = minifyCss;
exports.compile = compileSass;
exports.default = watchSass;